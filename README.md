# Overview for laps
Laps is an open-source implementation of LAPS (Local Administrator Password Solution) for GNU/Linux.

This [repository](https://gitlab.com/bgstack15/laps) is the home for laps4linux, or just *laps*. The [full readme](src/usr/share/doc/laps/README.md) is available farther down in the source tree.
